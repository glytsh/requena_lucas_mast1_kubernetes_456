# TP REQUENA_Lucas_Mast1_Kubernetes_456


## Chart Helm 

L'utilisation des Charts HELM se font via le package registry de Gitlab ou en ajoutant le dépot.

Registries : https://gitlab.com/glytsh/requena_lucas_mast1_kubernetes_456/-/packages

## Ajouter un dépôt Gitlab dans Helm 

```
$ helm repo add orchestration https://gitlab.com/api/v4/projects/34198605/packages/helm/stable
$ helm repo update
```

## Installation du Chart 

```
helm install lenomquetuveux requena_lucas_mast1_kubernetes_456/test-chart -f ./values.yaml
```

## Débug 

Comme vu en cours voici la commande qui permettra de visualiser les outputs et detecter de potentiels problèmes

```
$ helm install deno orchestration/deno-webserver -f ./values.yaml --dry-run --debug
```